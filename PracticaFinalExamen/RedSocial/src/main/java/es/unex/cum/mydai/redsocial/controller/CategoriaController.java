package es.unex.cum.mydai.redsocial.controller;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import es.unex.cum.mydai.redsocial.services.CategoriaService;
import es.unex.cum.mydai.redsocial.services.TemporalService;
import es.unex.cum.mydai.redsocial.vo.CategoriaVO;
import es.unex.cum.mydai.redsocial.vo.TemporalVO;

@Controller
public class CategoriaController {
	private final CategoriaService categoriaService;
	private final TemporalService temporalService;
	
	@Autowired
	public CategoriaController(CategoriaService categoriaService, TemporalService temporalService) {
		this.categoriaService = categoriaService;
		this.temporalService = temporalService;
	}
	
	@RequestMapping(value="/categorias", method = RequestMethod.GET)
	public String viewCategorias(ModelMap model, HttpSession session) {
		model.put("categorias", this.categoriaService.allCategorias());
		return "categorias";
	}
	
	@RequestMapping(value="/c/{cat}", method = RequestMethod.GET)
	public String profileCategoria(@PathVariable("cat") String cat, ModelMap model, HttpSession session) {
		TemporalVO temporal = new TemporalVO();
		model.addAttribute("mensaje", temporal);
		CategoriaVO categoria = this.categoriaService.findbyName(cat);
		model.addAttribute("listamensajes", this.temporalService.mensajesCategoria(categoria));
		model.addAttribute("categoria", categoria);
		return "profcategoria";
	}
}
