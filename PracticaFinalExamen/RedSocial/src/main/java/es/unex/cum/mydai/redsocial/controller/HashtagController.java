package es.unex.cum.mydai.redsocial.controller;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import es.unex.cum.mydai.redsocial.services.HashtagService;
import es.unex.cum.mydai.redsocial.vo.HashtagVO;
import es.unex.cum.mydai.redsocial.vo.UsuarioVO;

@Controller
public class HashtagController {
	private final HashtagService hashtagService;
	
	@Autowired
	public HashtagController(HashtagService hashtagService) {
		this.hashtagService = hashtagService;
	}
	
	@RequestMapping(value="/hashtag/{tag}", method = RequestMethod.GET)
	public String viewHashtag(@PathVariable("tag") String tag, ModelMap model, HttpSession session) {
		UsuarioVO user = (UsuarioVO) session.getAttribute("activeUser");
		if(user == null) {
			return "redirect:/login";
		} else {
			HashtagVO hashtag = hashtagService.findbyTag(tag);
			if(hashtag == null) {
				return "redirect:/timeline";
			} else {
				model.put("hashtag", hashtag);
				return "hashtag";
			}
		}
	}
	
}
