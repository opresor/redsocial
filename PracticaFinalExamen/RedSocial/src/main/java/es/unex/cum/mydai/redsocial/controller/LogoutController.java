package es.unex.cum.mydai.redsocial.controller;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import es.unex.cum.mydai.redsocial.services.UsuarioService;

@RequestMapping("/logout")
public class LogoutController {
	
private final UsuarioService usuarioService;
	
	@Autowired
	public LogoutController(UsuarioService usuarioService) {
		this.usuarioService = usuarioService;
	}
	
	@RequestMapping(method=RequestMethod.GET)
	public String logout(HttpSession session) {
		if(session.getAttribute("activeUser") != null) {
			session.removeAttribute("activeUser");
			return "redirect:/index";
		} else {
			return "redirect:/index";
		}
	}

}
