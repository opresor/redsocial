package es.unex.cum.mydai.redsocial.controller;

import java.util.Date;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import es.unex.cum.mydai.redsocial.services.HashtagService;
import es.unex.cum.mydai.redsocial.services.MensajeService;
import es.unex.cum.mydai.redsocial.vo.HashtagVO;
import es.unex.cum.mydai.redsocial.vo.MensajeVO;
import es.unex.cum.mydai.redsocial.vo.UsuarioVO;

@Controller
public class MensajeController {
	private final MensajeService mensajeService;
	private final HashtagService hashtagService;
	
	@Autowired
	public MensajeController(MensajeService mensajeService, HashtagService hashtagService) {
		this.mensajeService = mensajeService;
		this.hashtagService = hashtagService;
	}
	
	@RequestMapping(value="/publicar", method=RequestMethod.POST)
	public String publicar(@ModelAttribute("mensaje") MensajeVO mensaje, HttpSession session) {
		Date d = new Date();
		if(mensaje.getHashtag() == null) {
			mensaje.setSender((UsuarioVO) session.getAttribute("activeUser"));
			mensaje.setFecha(d);
			this.mensajeService.addMensaje(mensaje);
			return "redirect:/timeline";
		} else {
			HashtagVO tag = this.hashtagService.findbyTag(mensaje.getHashtag().getTag());
			if(tag == null) {
				HashtagVO hashtag = new HashtagVO();
				hashtag.setTag(mensaje.getHashtag().getTag());
				HashtagVO msjtag = this.hashtagService.addHashtag(hashtag);
				mensaje.setSender((UsuarioVO) session.getAttribute("activeUser"));
				mensaje.setFecha(d);
				mensaje.setHashtag(msjtag);
				this.mensajeService.addMensaje(mensaje);
				return "redirect:/timeline";
			} else {
				mensaje.setSender((UsuarioVO) session.getAttribute("activeUser"));
				mensaje.setFecha(d);
				mensaje.setHashtag(tag);
				this.mensajeService.addMensaje(mensaje);
				return "redirect:/timeline";
			}
		}
	}
}
