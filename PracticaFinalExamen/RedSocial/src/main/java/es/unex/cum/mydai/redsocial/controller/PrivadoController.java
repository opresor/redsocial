package es.unex.cum.mydai.redsocial.controller;

import java.util.Date;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import es.unex.cum.mydai.redsocial.services.PrivadoService;
import es.unex.cum.mydai.redsocial.services.UsuarioService;
import es.unex.cum.mydai.redsocial.vo.PrivadoVO;
import es.unex.cum.mydai.redsocial.vo.UsuarioVO;

@Controller
public class PrivadoController {
	private final PrivadoService privadoService;
	private final UsuarioService usuarioService;
	
	@Autowired
	public PrivadoController(PrivadoService privadoService, UsuarioService usuarioService) {
		this.privadoService = privadoService;
		this.usuarioService = usuarioService;
	}
	
	@RequestMapping(value="/enviarprivado/{id}", method = RequestMethod.POST)
	public String enviarPrivado(@ModelAttribute("privado") PrivadoVO privado, @PathVariable("id") Long id, HttpSession session) {
		PrivadoVO mensaje = new PrivadoVO();
		Date d = new Date();
		mensaje.setFecha(d);
		UsuarioVO user = (UsuarioVO) session.getAttribute("activeUser");
		Long idsender = user.getId();
		UsuarioVO receiver = usuarioService.findbyId(id);
		System.out.println(receiver.toString());
		mensaje.setReceiver(receiver);
		UsuarioVO sender = usuarioService.findbyId(idsender);
		System.out.println(sender.toString());
		mensaje.setSender(sender);
		mensaje.setBody(privado.getBody());
		privadoService.addPrivado(mensaje);
		return "redirect:/timeline";
	}
	
	@RequestMapping(value="/verprivados", method = RequestMethod.GET)
	public String viewPrivado(ModelMap model, HttpSession session) {
		UsuarioVO user = (UsuarioVO) session.getAttribute("activeUser");
		if(user == null) {
			return "redirect:/";
		} else {
			model.put("privados", this.privadoService.cargarConversacion(user));
			return "privados";
		}
	}
}
