package es.unex.cum.mydai.redsocial.controller;

import java.util.Calendar;
import java.util.Date;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import es.unex.cum.mydai.redsocial.services.CategoriaService;
import es.unex.cum.mydai.redsocial.services.TemporalService;
import es.unex.cum.mydai.redsocial.vo.CategoriaVO;
import es.unex.cum.mydai.redsocial.vo.TemporalVO;
import es.unex.cum.mydai.redsocial.vo.UsuarioVO;

@Controller
public class TemporalController {
	private final TemporalService temporalService;
	private final CategoriaService categoriaService;
	
	@Autowired
	public TemporalController(TemporalService temporalService, CategoriaService categoriaService) {
		this.temporalService = temporalService;
		this.categoriaService = categoriaService;
	}
	
	@RequestMapping(value="c/temporalpubli/{categoriaid}", method = RequestMethod.POST)
	public String publicarTemporal(@ModelAttribute("mensaje") TemporalVO temporal, @PathVariable("categoriaid") Long id, HttpSession session) {
		UsuarioVO user = (UsuarioVO) session.getAttribute("activeUser");
		if(user == null) {
			return "redirect:/login";
		} else {
			Date d = new Date();
			Calendar c = Calendar.getInstance();
			c.setTime(d);
			c.add(Calendar.DATE, temporal.getDias());
			d = c.getTime();
			System.out.println("ID de la Categoria: "+id);
			CategoriaVO categ = new CategoriaVO();
			categ = this.categoriaService.findbyId(id);
			temporal.setCategoria(categ);
			temporal.setFecha_max(d);
			temporal.setSender(user);
			this.temporalService.addTemporal(temporal);
			return "redirect:/c/"+categ.getNombre();
		}
	}
	
}
