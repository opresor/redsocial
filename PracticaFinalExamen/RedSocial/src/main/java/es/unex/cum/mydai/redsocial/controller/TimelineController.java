package es.unex.cum.mydai.redsocial.controller;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import es.unex.cum.mydai.redsocial.services.MensajeService;
import es.unex.cum.mydai.redsocial.services.UsuarioService;
import es.unex.cum.mydai.redsocial.vo.MensajeVO;
import es.unex.cum.mydai.redsocial.vo.UsuarioVO;

@Controller
public class TimelineController {
	
	private final UsuarioService usuarioService;
	private final MensajeService mensajeService;
	
	@Autowired
	public TimelineController(UsuarioService usuarioService, MensajeService mensajeService) {
		this.mensajeService = mensajeService;
		this.usuarioService = usuarioService;
	}
	
	@RequestMapping(value="/timeline", method=RequestMethod.GET)
	public String viewTimeline(ModelMap model, HttpSession session) {
		UsuarioVO log = (UsuarioVO) session.getAttribute("activeUser");
		if(log == null) {
			return "redirect:/login";
		} else {
		model.put("mensaje", new MensajeVO());
		model.put("timeline", mensajeService.findTimeline(log.getId()));
		return "timeline";
		}
		
	}

}
