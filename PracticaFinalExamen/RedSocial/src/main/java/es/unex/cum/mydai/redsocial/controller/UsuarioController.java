package es.unex.cum.mydai.redsocial.controller;

import java.util.Date;

import javax.servlet.http.HttpSession;

import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import es.unex.cum.mydai.redsocial.services.MensajeService;
import es.unex.cum.mydai.redsocial.services.UsuarioService;
import es.unex.cum.mydai.redsocial.vo.MensajeVO;
import es.unex.cum.mydai.redsocial.vo.PrivadoVO;
import es.unex.cum.mydai.redsocial.vo.UsuarioVO;

@Controller
public class UsuarioController {
	private final UsuarioService usuarioService;
	private final MensajeService mensajeService;
	
	@Autowired
	public UsuarioController(UsuarioService usuarioService, MensajeService mensajeService) {
		this.usuarioService = usuarioService;
		this.mensajeService = mensajeService;
	}
	
	@RequestMapping(value ="/")
	public String home() {
		System.out.println("Entrando..\n");
		return "index";
	}
	
	@RequestMapping(value ="/registro", method = RequestMethod.GET)
	public String viewRegistrar(ModelMap model) {
		UsuarioVO usuario = new UsuarioVO();
		model.put("usuario", usuario);
		return "registro";
	}
	
	@RequestMapping(value="/registro", method = RequestMethod.POST)
	public String registrar(@ModelAttribute("usuario") UsuarioVO usuario, ModelMap model, HttpSession session) {
		UsuarioVO registrado = this.usuarioService.login(usuario.getUsername());
		if(registrado == null) {
			Date d = new Date();
			usuario.setFechaRegistro(d);
			this.usuarioService.insertarUsuario(usuario);
		} else {
			model.put("usuario", new UsuarioVO());
			return "registro";
		}
		return "redirect:/login";
	}
	
	@RequestMapping(value="/login", method=RequestMethod.GET)
	public String viewLogin(ModelMap model) {
		model.put("usuario", new UsuarioVO());
		return "login";
	}
	
	@RequestMapping(value="/login", method=RequestMethod.POST)
	public String login(@ModelAttribute("usuario") UsuarioVO usuario, ModelMap model, HttpSession session) {
		UsuarioVO existe = this.usuarioService.login(usuario.getUsername());
		if(existe == null) {
			return "login";
		} else {
			if(existe.getPassword().equals(usuario.getPassword())) {
				session.setAttribute("activeUser", existe);
				return "redirect:/timeline";
			} else {
				return "login";
			}
		}
	}	
	
	@RequestMapping(value="/logout", method=RequestMethod.GET)
	public String logout(HttpSession session) {
		if(session.getAttribute("activeUser") != null) {
			session.removeAttribute("activeUser");
			return "index";
		} else {
			return "index";
		}
	}
	
	@RequestMapping(value="/{userid}", method=RequestMethod.GET)
	public ModelAndView viewProfile(@PathVariable("userid") Long id, ModelMap model, HttpSession session) {
		ModelAndView mav = new ModelAndView("profile");
		UsuarioVO profile = this.usuarioService.findbyId(id);
		PrivadoVO privado = new PrivadoVO();
		if(profile == null) {
			return mav;
		} else {
			mav.addObject("profile", profile);
			mav.addObject("timeline", this.mensajeService.findUserMensajes(profile.getId()));
			mav.addObject("privado", privado);
			return mav;
		}
	}
		
	/*@RequestMapping(value="/profile/{userid}", method=RequestMethod.GET)
	public String viewProfile(@PathVariable("userid") Long id, ModelMap model, HttpSession session) {
		UsuarioVO profile = this.usuarioService.findbyId(id);
		if(profile == null) {
			return "redirect:/timeline";
		} else {
			model.put("profile", profile);
			model.put("timeline", this.mensajeService.findUserMensajes(profile.getId()));
			return "profile";
		}
	}*/
	
	@RequestMapping(value="/profile/follow/{userid}", method=RequestMethod.GET)
	public String followUser(@PathVariable("userid") Long id, HttpSession session) {
		UsuarioVO toFollow = this.usuarioService.findbyId(id);
	
		if(toFollow != null) {
			UsuarioVO user = (UsuarioVO) session.getAttribute("activeUser");
			System.out.println(user.getUsername());
			toFollow.addFollower(user);
			this.usuarioService.updateUsuario(toFollow);
			//user = this.usuarioService.updateUsuario(user);
			session.setAttribute("activeUser", user);
			return "redirect:/timeline";
			
		}
		return "redirect:/timeline";
	}

	
}
