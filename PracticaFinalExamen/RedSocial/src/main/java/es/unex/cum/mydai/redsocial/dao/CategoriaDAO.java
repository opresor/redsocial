package es.unex.cum.mydai.redsocial.dao;

import java.util.List;
import java.util.Set;

import es.unex.cum.mydai.redsocial.vo.CategoriaVO;

public interface CategoriaDAO {
	CategoriaVO create(CategoriaVO categoria);
	CategoriaVO find(Long id);
	CategoriaVO update(CategoriaVO categoria);
	void delete(CategoriaVO categoria);
	Set<CategoriaVO> findCategorias();
	CategoriaVO getCategoriaMensajes(String name);
}
