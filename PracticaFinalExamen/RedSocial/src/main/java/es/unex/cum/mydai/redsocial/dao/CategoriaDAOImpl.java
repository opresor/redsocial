package es.unex.cum.mydai.redsocial.dao;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import es.unex.cum.mydai.redsocial.vo.CategoriaVO;

@Component
@Transactional
public class CategoriaDAOImpl implements CategoriaDAO {

	@PersistenceContext
	protected EntityManager entityManager;
	
	@Override
	public CategoriaVO create(CategoriaVO categoria) {
		this.entityManager.persist(categoria);
		return categoria;
	}

	@Override
	public CategoriaVO find(Long id) {
		return this.entityManager.find(CategoriaVO.class, id);
	}

	@Override
	public CategoriaVO update(CategoriaVO categoria) {
		this.entityManager.merge(categoria);
		return categoria;
	}

	@Override
	public void delete(CategoriaVO categoria) {
		categoria = this.entityManager.merge(categoria);
		this.entityManager.remove(categoria);
	}

	@SuppressWarnings("unchecked")
	@Override
	public Set<CategoriaVO> findCategorias() {
		Query query = this.entityManager.createQuery(
				"SELECT c FROM CategoriaVO c", CategoriaVO.class);
		return new HashSet<CategoriaVO>(query.getResultList());
	}

	@Override
	public CategoriaVO getCategoriaMensajes(String name) {
		CategoriaVO categoria = null;
		try {
			Query query = this.entityManager.createQuery(
					"SELECT c FROM CategoriaVO c WHERE c.nombre = ?1");
			query.setParameter(1, name);
			categoria = (CategoriaVO) query.getSingleResult();
		} catch (NoResultException e) {
			
		}
		return categoria;
	}

}
