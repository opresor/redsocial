package es.unex.cum.mydai.redsocial.dao;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import es.unex.cum.mydai.redsocial.vo.HashtagVO;

public interface HashtagDAO {
	HashtagVO create(HashtagVO hashtag);
	HashtagVO update(HashtagVO hashtag);
	HashtagVO find(Long id);
	void delete(HashtagVO hashtag);
	HashtagVO findByHashtag(String hashtag);
}
