package es.unex.cum.mydai.redsocial.dao;


import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.Table;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import es.unex.cum.mydai.redsocial.vo.HashtagVO;

@Component
@Transactional
public class HashtagDAOImpl implements HashtagDAO {
	
	@PersistenceContext
	protected EntityManager entityManager;
/*
	public HashtagDAOImpl(EntityManager entityManager) {
		this.entityManager = entityManager;
	}*/

	public HashtagVO create(HashtagVO hashtag) {
		this.entityManager.persist(hashtag);
		return hashtag;
	}

	public HashtagVO update(HashtagVO hashtag) {
		hashtag = this.entityManager.merge(hashtag);
		return hashtag;
	}

	public HashtagVO find(Long id) {
		return this.entityManager.find(HashtagVO.class, id);
	}

	public void delete(HashtagVO hashtag) {
		hashtag = this.entityManager.merge(hashtag);
		this.entityManager.remove(hashtag);

	}

	public HashtagVO findByHashtag(String hashtag) {
		
		HashtagVO htag = null;
		try {
			Query query = this.entityManager.createQuery(
				"SELECT h FROM HashtagVO h WHERE h.tag = ?1", HashtagVO.class);
			query.setParameter(1, hashtag);
			htag = (HashtagVO) query.getSingleResult();
		} catch (NoResultException e) {
			
		}
		return htag;
	}

}
