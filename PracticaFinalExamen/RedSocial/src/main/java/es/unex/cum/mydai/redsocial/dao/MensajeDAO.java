package es.unex.cum.mydai.redsocial.dao;

import java.util.List;

import es.unex.cum.mydai.redsocial.vo.MensajeVO;

public interface MensajeDAO {
	MensajeVO create(MensajeVO mensaje);
	MensajeVO find(Long id);
	MensajeVO update(MensajeVO mensaje);
	void delete(MensajeVO mensaje);
	List<MensajeVO> findByUser(Long userid);
	List<MensajeVO> findTimeline(Long userid);
}
