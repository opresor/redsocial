package es.unex.cum.mydai.redsocial.dao;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import javax.persistence.Table;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import es.unex.cum.mydai.redsocial.vo.MensajeVO;
import es.unex.cum.mydai.redsocial.vo.UsuarioVO;

@Component
@Transactional
public class MensajeDAOImpl implements MensajeDAO {
	
	@PersistenceContext
	protected EntityManager entityManager;

	/*public MensajeDAOImpl(EntityManager entityManager) {
		this.entityManager = entityManager;
	}*/

	public MensajeVO create(MensajeVO mensaje) {
		this.entityManager.persist(mensaje);
		return mensaje;
	}

	public MensajeVO find(Long id) {
		return this.entityManager.find(MensajeVO.class, id);
	}

	public MensajeVO update(MensajeVO mensaje) {
		this.entityManager.merge(mensaje);
		return mensaje;
	}

	public void delete(MensajeVO mensaje) {
		mensaje = this.entityManager.merge(mensaje);
		this.entityManager.remove(mensaje);

	}

	@SuppressWarnings("unchecked")
	public List<MensajeVO> findByUser(Long userid) {
		Query query = this.entityManager.createQuery(
				"SELECT m FROM MensajeVO m WHERE m.sender.id = ?1 ORDER BY m.fecha DESC", MensajeVO.class);
		query.setParameter(1, userid);
		return new ArrayList<MensajeVO>(query.getResultList());
	}
	
	@SuppressWarnings("unchecked")
	public List<MensajeVO> findTimeline(Long userid) {
		Query query = this.entityManager.createQuery(
					"SELECT m FROM MensajeVO m WHERE EXISTS(SELECT 1 FROM UsuarioVO u JOIN u.followers fr WHERE fr.id = ?1) OR m.sender.id = ?2 ORDER BY m.fecha DESC", MensajeVO.class);
		query.setParameter(1, userid);
		query.setParameter(2, userid);
					
		return new ArrayList<MensajeVO>(query.getResultList());
		
		//SELECT m FROM `mensajes` m where exists (SELECT 1 FROM user_follow u WHERE u.FOLLOWED_ID = m.ID_SENDER AND u.FOLLOWER_ID = 1
		// SELECT * FROM `mensajes` INNER JOIN `user_follow` ON mensajes.ID_SENDER = user_follow.FOLLOWED_ID WHERE user_follow.FOLLOWER_ID = 1
				
	}
}
