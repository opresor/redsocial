package es.unex.cum.mydai.redsocial.dao;

import java.util.List;
import java.util.Set;

import es.unex.cum.mydai.redsocial.vo.PrivadoVO;
import es.unex.cum.mydai.redsocial.vo.UsuarioVO;

public interface PrivadoDAO {
	PrivadoVO create(PrivadoVO mensaje);
	PrivadoVO find(Long id);
	PrivadoVO update(PrivadoVO mensaje);
	void delete(PrivadoVO mensaje);
	List<PrivadoVO> findConversation(UsuarioVO user);
}
