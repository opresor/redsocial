package es.unex.cum.mydai.redsocial.dao;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.Table;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import es.unex.cum.mydai.redsocial.vo.PrivadoVO;
import es.unex.cum.mydai.redsocial.vo.UsuarioVO;

@Component
@Transactional
public class PrivadoDAOImpl implements PrivadoDAO {
	
	@PersistenceContext
	protected EntityManager entityManager;

	/*public PrivadoDAOImpl(EntityManager entityManager) {
		this.entityManager = entityManager;
	}*/

	public PrivadoVO create(PrivadoVO mensaje) {
		this.entityManager.persist(mensaje);
		return mensaje;
	}

	public PrivadoVO find(Long id) {
		return this.entityManager.find(PrivadoVO.class, id);
	}

	public PrivadoVO update(PrivadoVO mensaje) {
		this.entityManager.merge(mensaje);
		return mensaje;
	}

	public void delete(PrivadoVO mensaje) {
		mensaje = this.entityManager.merge(mensaje);
		this.entityManager.remove(mensaje);
	}

	@SuppressWarnings("unchecked")
	public List<PrivadoVO> findConversation(UsuarioVO user) {
		Query query = this.entityManager.createQuery(
				"SELECT p FROM PrivadoVO p WHERE p.sender = ?1 OR p.receiver = ?2 ORDER BY fecha_envio DESC",
				PrivadoVO.class);
		query.setParameter(1, user);
		query.setParameter(2, user);
		return new ArrayList<PrivadoVO>(query.getResultList());
	}

}
