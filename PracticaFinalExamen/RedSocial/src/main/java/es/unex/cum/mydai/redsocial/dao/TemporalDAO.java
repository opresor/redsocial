package es.unex.cum.mydai.redsocial.dao;

import java.util.List;

import es.unex.cum.mydai.redsocial.vo.CategoriaVO;
import es.unex.cum.mydai.redsocial.vo.TemporalVO;

public interface TemporalDAO {
	TemporalVO create(TemporalVO mensaje);
	TemporalVO find(Long id);
	TemporalVO update(TemporalVO mensaje);
	void delete(TemporalVO mensaje);
	List<TemporalVO> findbyCategoria(CategoriaVO categoria);

}
