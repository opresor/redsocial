package es.unex.cum.mydai.redsocial.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import es.unex.cum.mydai.redsocial.vo.CategoriaVO;
import es.unex.cum.mydai.redsocial.vo.TemporalVO;

@Component
@Transactional
public class TemporalDAOImpl implements TemporalDAO {
	
	@PersistenceContext
	protected EntityManager entityManager;

	@Override
	public TemporalVO create(TemporalVO mensaje) {
		this.entityManager.persist(mensaje);
		return mensaje;
	}

	@Override
	public TemporalVO find(Long id) {
		return this.entityManager.find(TemporalVO.class, id);
	}

	@Override
	public TemporalVO update(TemporalVO mensaje) {
		this.entityManager.merge(mensaje);
		return mensaje;
	}

	@Override
	public void delete(TemporalVO mensaje) {
		mensaje = this.entityManager.merge(mensaje);
		this.entityManager.remove(mensaje);
	}
	
	@SuppressWarnings("unchecked")
	public List<TemporalVO> findbyCategoria(CategoriaVO categoria){
		Date d = new Date();
		Query query = this.entityManager.createQuery(
				"SELECT t FROM TemporalVO t WHERE t.categoria = ?1 AND t.fecha_max >= ?2 ORDER BY t.fecha_max DESC");
		query.setParameter(1, categoria);
		query.setParameter(2, d);
		return new ArrayList<TemporalVO>(query.getResultList());
	}

}
