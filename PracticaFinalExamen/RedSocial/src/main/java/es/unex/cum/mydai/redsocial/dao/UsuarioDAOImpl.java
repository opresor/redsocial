package es.unex.cum.mydai.redsocial.dao;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.Table;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import es.unex.cum.mydai.redsocial.vo.UsuarioVO;

@Component
@Transactional
public class UsuarioDAOImpl implements UsuarioDAO {
	@PersistenceContext
	protected EntityManager entityManager;
	
	/*
	public UsuarioDAOImpl(EntityManager entityManager) {
		this.entityManager = entityManager;
	}*/

	public UsuarioVO create(UsuarioVO usuario) {
		this.entityManager.persist(usuario);
		return usuario;
	}

	public UsuarioVO read(Long id) {
		return this.entityManager.find(UsuarioVO.class, id);
	}

	public UsuarioVO update(UsuarioVO usuario) {
		this.entityManager.merge(usuario);
		return usuario;
	}

	public void delete(UsuarioVO usuario) {
		usuario = this.entityManager.merge(usuario);
		this.entityManager.remove(usuario);
	}

	@SuppressWarnings("unchecked")
	public Set<UsuarioVO> findByName(String name) {
		Query query = entityManager.createQuery(
				"SELECT u FROM UsuarioVO u WHERE u.name = ?1 OR u.username = ?2", UsuarioVO.class);
		query.setParameter(1, "%"+name+"%");
		query.setParameter(2, "%"+name+"%");
		return new HashSet<UsuarioVO>(query.getResultList());
	}
	
	public UsuarioVO login(String username) {
		UsuarioVO result = null;
		try {
		Query query = entityManager.createQuery(
				"SELECT u FROM UsuarioVO u WHERE u.username = ?1", UsuarioVO.class);
		query.setParameter(1, username);
		result = (UsuarioVO) query.getSingleResult();
		} catch(NoResultException e) {
			
		}
		return result;
	}

}
