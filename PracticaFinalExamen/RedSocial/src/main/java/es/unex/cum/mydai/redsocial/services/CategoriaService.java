package es.unex.cum.mydai.redsocial.services;

import java.util.Set;

import es.unex.cum.mydai.redsocial.vo.CategoriaVO;

public interface CategoriaService {
	public CategoriaVO insertarCategoria(CategoriaVO categoria);
	public CategoriaVO findbyId(Long id);
	public CategoriaVO editarCategoria(CategoriaVO categoria);
	public void borrarCategoria(CategoriaVO categoria);
	public Set<CategoriaVO> allCategorias();
	public CategoriaVO findbyName(String name);
}
