package es.unex.cum.mydai.redsocial.services;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import es.unex.cum.mydai.redsocial.dao.CategoriaDAO;
import es.unex.cum.mydai.redsocial.vo.CategoriaVO;

@Component
public class CategoriaServiceImpl implements CategoriaService {
	
	private CategoriaDAO categoriaDAO;
	
	

	public CategoriaDAO getCategoriaDAO() {
		return categoriaDAO;
	}

	
	@Autowired
	public void setCategoriaDAO(CategoriaDAO categoriaDAO) {
		this.categoriaDAO = categoriaDAO;
	}

	@Override
	public CategoriaVO insertarCategoria(CategoriaVO categoria) {
		return this.categoriaDAO.create(categoria);
	}

	@Override
	public CategoriaVO findbyId(Long id) {
		return this.categoriaDAO.find(id);
	}

	@Override
	public CategoriaVO editarCategoria(CategoriaVO categoria) {
		return this.categoriaDAO.update(categoria);
	}

	@Override
	public void borrarCategoria(CategoriaVO categoria) {
		this.categoriaDAO.delete(categoria);

	}

	@Override
	public Set<CategoriaVO> allCategorias() {
		return this.categoriaDAO.findCategorias();
	}

	@Override
	public CategoriaVO findbyName(String name) {
		return this.categoriaDAO.getCategoriaMensajes(name);
	}

}
