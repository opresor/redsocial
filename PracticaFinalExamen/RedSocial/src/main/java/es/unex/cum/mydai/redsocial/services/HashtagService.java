package es.unex.cum.mydai.redsocial.services;

import es.unex.cum.mydai.redsocial.vo.HashtagVO;

public interface HashtagService {
	
	public HashtagVO addHashtag(HashtagVO hashtag);
	public HashtagVO findbyId(Long id);
	public HashtagVO editarHashtag(HashtagVO hashtag);
	public void borrarHashtag(HashtagVO hashtag);
	public HashtagVO findbyTag(String tag);

}
