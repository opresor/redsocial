package es.unex.cum.mydai.redsocial.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import es.unex.cum.mydai.redsocial.dao.HashtagDAO;
import es.unex.cum.mydai.redsocial.vo.HashtagVO;

@Component
public class HashtagServiceImpl implements HashtagService {
	
	private HashtagDAO hashtagDAO;
	
	

	public HashtagDAO getHashtagDAO() {
		return hashtagDAO;
	}
	
	@Autowired
	public void setHashtagDAO(HashtagDAO hashtagDAO) {
		this.hashtagDAO = hashtagDAO;
	}

	public HashtagVO addHashtag(HashtagVO hashtag) {
		return this.hashtagDAO.create(hashtag);
	}

	public HashtagVO findbyId(Long id) {
		return this.hashtagDAO.find(id);
	}

	public HashtagVO editarHashtag(HashtagVO hashtag) {
		return this.hashtagDAO.update(hashtag);
	}

	public void borrarHashtag(HashtagVO hashtag) {
		this.hashtagDAO.delete(hashtag);

	}

	public HashtagVO findbyTag(String tag) {
		return this.hashtagDAO.findByHashtag(tag);
	}

}
