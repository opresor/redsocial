package es.unex.cum.mydai.redsocial.services;

import java.util.List;
import java.util.Set;

import es.unex.cum.mydai.redsocial.vo.MensajeVO;

public interface MensajeService {
	
	public MensajeVO addMensaje(MensajeVO mensaje);
	public MensajeVO findbyId(Long id);
	public MensajeVO editarMensaje(MensajeVO mensaje);
	public void borrarMensaje(MensajeVO mensaje);
	public List<MensajeVO> findUserMensajes(Long id);
	public List<MensajeVO> findTimeline(Long userid);

}
