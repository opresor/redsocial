package es.unex.cum.mydai.redsocial.services;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import es.unex.cum.mydai.redsocial.dao.MensajeDAO;
import es.unex.cum.mydai.redsocial.vo.MensajeVO;

@Component
public class MensajeServiceImpl implements MensajeService {
	
	private MensajeDAO mensajeDAO;
	
	
	public MensajeServiceImpl() {}
	
	

	public MensajeDAO getMensajeDAO() {
		return mensajeDAO;
	}

	@Autowired
	public void setMensajeDAO(MensajeDAO mensajeDAO) {
		this.mensajeDAO = mensajeDAO;
	}



	public MensajeVO addMensaje(MensajeVO mensaje) {
		return this.mensajeDAO.create(mensaje);
	}

	public MensajeVO findbyId(Long id) {
		return this.mensajeDAO.find(id);
	}

	public MensajeVO editarMensaje(MensajeVO mensaje) {
		return this.mensajeDAO.update(mensaje);
	}

	public void borrarMensaje(MensajeVO mensaje) {
		this.mensajeDAO.delete(mensaje);

	}

	public List<MensajeVO> findUserMensajes(Long id) {
		return this.mensajeDAO.findByUser(id);
	}
	
	public List<MensajeVO> findTimeline(Long userid){
		return this.mensajeDAO.findTimeline(userid);
	}

}
