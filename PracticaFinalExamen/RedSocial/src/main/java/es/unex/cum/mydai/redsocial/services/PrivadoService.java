package es.unex.cum.mydai.redsocial.services;

import java.util.List;
import java.util.Set;

import es.unex.cum.mydai.redsocial.vo.PrivadoVO;
import es.unex.cum.mydai.redsocial.vo.UsuarioVO;

public interface PrivadoService {
	
	public PrivadoVO addPrivado(PrivadoVO privado);
	public PrivadoVO findbyId(Long id);
	public PrivadoVO editarPrivado(PrivadoVO privado);
	public void borrarPrivado(PrivadoVO privado);
	public List<PrivadoVO> cargarConversacion(UsuarioVO user);

}
