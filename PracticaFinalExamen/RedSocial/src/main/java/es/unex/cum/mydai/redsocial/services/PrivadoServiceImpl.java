package es.unex.cum.mydai.redsocial.services;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import es.unex.cum.mydai.redsocial.dao.PrivadoDAO;
import es.unex.cum.mydai.redsocial.vo.PrivadoVO;
import es.unex.cum.mydai.redsocial.vo.UsuarioVO;

@Component
public class PrivadoServiceImpl implements PrivadoService {
	
	private PrivadoDAO privadoDAO;
	
	public PrivadoServiceImpl() {}
	
	public PrivadoDAO getPrivadoDAO() {
		return privadoDAO;
	}
	
	@Autowired
	public void setPrivadoDAO(PrivadoDAO privadoDAO) {
		this.privadoDAO = privadoDAO;
	}

	public PrivadoVO addPrivado(PrivadoVO privado) {
		return this.privadoDAO.create(privado);
	}

	public PrivadoVO findbyId(Long id) {
		return this.privadoDAO.find(id);
	}

	public PrivadoVO editarPrivado(PrivadoVO privado) {
		return this.privadoDAO.update(privado);
	}

	public void borrarPrivado(PrivadoVO privado) {
		this.privadoDAO.delete(privado);

	}

	public List<PrivadoVO> cargarConversacion(UsuarioVO user) {
		return this.privadoDAO.findConversation(user);
	}

}
