package es.unex.cum.mydai.redsocial.services;

import java.util.List;

import es.unex.cum.mydai.redsocial.vo.CategoriaVO;
import es.unex.cum.mydai.redsocial.vo.TemporalVO;

public interface TemporalService {
	public TemporalVO addTemporal(TemporalVO mensaje);
	public TemporalVO findbyId(Long id);
	public TemporalVO editarMensaje(TemporalVO mensaje);
	public void borrarMensaje(TemporalVO mensaje);
	public List<TemporalVO> mensajesCategoria(CategoriaVO categoria);
}
