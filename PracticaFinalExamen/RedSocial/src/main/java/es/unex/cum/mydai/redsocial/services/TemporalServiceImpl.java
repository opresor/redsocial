package es.unex.cum.mydai.redsocial.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import es.unex.cum.mydai.redsocial.dao.TemporalDAO;
import es.unex.cum.mydai.redsocial.vo.CategoriaVO;
import es.unex.cum.mydai.redsocial.vo.TemporalVO;

@Component
public class TemporalServiceImpl implements TemporalService {
	
	private TemporalDAO temporalDAO;
	
	

	public TemporalDAO getTemporalDAO() {
		return temporalDAO;
	}
	
	@Autowired
	public void setTemporalDAO(TemporalDAO temporalDAO) {
		this.temporalDAO = temporalDAO;
	}

	@Override
	public TemporalVO addTemporal(TemporalVO mensaje) {
		return this.temporalDAO.create(mensaje);
	}

	@Override
	public TemporalVO findbyId(Long id) {
		return this.temporalDAO.find(id);
	}

	@Override
	public TemporalVO editarMensaje(TemporalVO mensaje) {
		return this.temporalDAO.update(mensaje);
	}

	@Override
	public void borrarMensaje(TemporalVO mensaje) {
		this.temporalDAO.delete(mensaje);

	}

	@Override
	public List<TemporalVO> mensajesCategoria(CategoriaVO categoria) {
		return this.temporalDAO.findbyCategoria(categoria);
	}

}
