package es.unex.cum.mydai.redsocial.services;

import java.util.Set;

import es.unex.cum.mydai.redsocial.vo.UsuarioVO;

public interface UsuarioService {
	
	public UsuarioVO insertarUsuario(UsuarioVO usuario);
	public UsuarioVO findbyId(Long id);
	public UsuarioVO updateUsuario(UsuarioVO usuario);
	public void borrarUsuario(UsuarioVO usuario);
	public Set<UsuarioVO> findbyName(String name);
	public UsuarioVO login(String username);

}
