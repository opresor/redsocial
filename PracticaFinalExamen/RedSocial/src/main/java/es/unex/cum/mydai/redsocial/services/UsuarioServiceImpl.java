package es.unex.cum.mydai.redsocial.services;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import es.unex.cum.mydai.redsocial.dao.UsuarioDAO;
import es.unex.cum.mydai.redsocial.vo.UsuarioVO;

@Component
public class UsuarioServiceImpl implements UsuarioService {
	
	private UsuarioDAO usuarioDAO;
	
	public UsuarioServiceImpl() {}
	
	
	public UsuarioDAO getUsuarioDAO() {
		return usuarioDAO;
	}


	@Autowired
	public void setUsuarioDAO(UsuarioDAO usuarioDAO) {
		this.usuarioDAO = usuarioDAO;
	}



	public UsuarioVO insertarUsuario(UsuarioVO usuario) {
		return this.usuarioDAO.create(usuario);
	}

	public UsuarioVO findbyId(Long id) {
		return this.usuarioDAO.read(id);
	}

	public UsuarioVO updateUsuario(UsuarioVO usuario) {
		return this.usuarioDAO.update(usuario);
	}

	public void borrarUsuario(UsuarioVO usuario) {
		this.usuarioDAO.delete(usuario);

	}

	public Set<UsuarioVO> findbyName(String name) {
		return this.usuarioDAO.findByName(name);
	}

	public UsuarioVO login(String username) {
		return this.usuarioDAO.login(username);
	}

}
