package es.unex.cum.mydai.redsocial.vo;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "CATEGORIA")
public class CategoriaVO implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 8933091049910276811L;
	private Long id;
	private String nombre;
	private Set<TemporalVO> mensajes;
	
	public CategoriaVO() {}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "NOMBRE")
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	@OneToMany(mappedBy = "categoria", fetch = FetchType.EAGER)
	public Set<TemporalVO> getMensajes() {
		return mensajes;
	}

	public void setMensajes(Set<TemporalVO> mensajes) {
		this.mensajes = mensajes;
	}
	
	
}
