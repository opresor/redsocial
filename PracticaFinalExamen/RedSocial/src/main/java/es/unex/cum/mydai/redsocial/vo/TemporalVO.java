package es.unex.cum.mydai.redsocial.vo;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "TEMPORALES")
public class TemporalVO implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -6911263504422402256L;
	private Long id;
	private UsuarioVO sender;
	private String body;
	private Date fecha_max;
	private int dias;
	private CategoriaVO categoria;
	
	public TemporalVO() {}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name = "ID_SENDER")
	public UsuarioVO getSender() {
		return sender;
	}

	public void setSender(UsuarioVO sender) {
		this.sender = sender;
	}

	@Column(name = "BODY")
	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHA_MAX")
	public Date getFecha_max() {
		return fecha_max;
	}

	public void setFecha_max(Date fecha_max) {
		this.fecha_max = fecha_max;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "ID_CATEGORIA")
	public CategoriaVO getCategoria() {
		return categoria;
	}

	public void setCategoria(CategoriaVO categoria) {
		System.out.println("Prueba\n");
		System.out.println(categoria.toString());
		this.categoria = categoria;
	}

	@Column(name="DISPONIBILIDAD")
	public int getDias() {
		return dias;
	}

	public void setDias(int dias) {
		this.dias = dias;
	}
	
	
	
	
	
	
}
