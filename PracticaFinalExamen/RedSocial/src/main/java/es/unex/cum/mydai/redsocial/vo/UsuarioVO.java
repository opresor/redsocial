package es.unex.cum.mydai.redsocial.vo;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table (name = "USUARIOS")
public class UsuarioVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6478288715736546720L;
	private Long id;
	private String username;
	private String name;
	private String email;
	private String password;
	private Date fechaRegistro;
	private String avatarurl;
	private Set<UsuarioVO> followers;
	private Set<UsuarioVO> following;
	
	public UsuarioVO() {}
	
	@Column(name = "USERNAME")
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}
	@Column(name = "NAME")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	@Column(name = "EMAIL")
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	@Column(name = "PASSWORD")
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Long getId() {
		return id;
	}
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHA_REGISTRO")
	public Date getFechaRegistro() {
		return fechaRegistro;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setFechaRegistro(Date fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}
	
	@Column(name = "AVATAR_IMG")
	public String getAvatarurl() {
		return avatarurl;
	}

	public void setAvatarurl(String avatarurl) {
		this.avatarurl = avatarurl;
	}

	@ManyToMany(cascade = CascadeType.PERSIST, fetch = FetchType.EAGER)
	@JoinTable(name = "USER_FOLLOW",
	joinColumns = @JoinColumn(name = "FOLLOWED_ID"),
	inverseJoinColumns = @JoinColumn(name = "FOLLOWER_ID"))
	public Set<UsuarioVO> getFollowers() {
		return followers;
	}

	public void setFollowers(Set<UsuarioVO> followers) {
		this.followers = followers;
	}
	
	public void addFollower(UsuarioVO user) {
		followers.add(user);
		user.following.add(this);
	}
	@ManyToMany(mappedBy = "followers", fetch = FetchType.EAGER)
	public Set<UsuarioVO> getFollowing() {
		return following;
	}

	public void setFollowing(Set<UsuarioVO> following) {
		this.following = following;
	}
	
	public void addFollowing(UsuarioVO user) {
		user.addFollower(this);
	}
	
	

}
