<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Categorias</title>
<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/css/style.css"/>" />
<script src="<c:url value="/resources/js/mensaje.js"/>"></script>
</head>
<body class="news">
	<header>
		<div class="nav">
			<ul>
				<li><a href="timeline">Timeline</a></li>
				<li><a href="<c:out value="${activeUser.id}"/>"><c:out
							value="${activeUser.name}" /></a></li>
				<li><a href="categorias">Categor�as</a></li>
				<li><a href="verprivados">Mensajes</a></li>
				<li><a href="logout">Cerrar sesi�n</a></li>
			</ul>
		</div>
	</header>
	<div id="timeline">
		<c:forEach var="categ" items="${categorias}">
			<b>Categor�a:</b> <a href="c/<c:out value="${categ.nombre}"/>"><c:out value="${categ.nombre}"/></a><br>
			<b>Publicaciones:</b> ${fn:length(categ.mensajes)}<hr>
		</c:forEach>
	</div>
</body>
</html>