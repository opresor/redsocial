<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Sign Up Form</title>
<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/css/loginreg.css"/>" />
</head>
<body>
<p class="texto">Bienvenido a tu Red Social</p>
<div class="Registro">
			<a href="registro"><input type="submit" value="Registrarse" title="Registra tu cuenta"></a>
			<a href="login"><input type="submit" value="Login" title="Login"></a>
</body>
</html>
