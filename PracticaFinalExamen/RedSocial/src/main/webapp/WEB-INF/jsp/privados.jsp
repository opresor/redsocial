<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Timeline</title>
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/style.css"/>" />
<script src="<c:url value="/resources/js/mensaje.js"/>"></script>
</head>
<body class="news">
	<header>
		<div class="nav">
			<ul>
				<li><a href="timeline">Timeline</a></li>
				<li><a href="<c:out value="${activeUser.id}"/>"><c:out
							value="${activeUser.name}" /></a></li>
				<li><a href="verprivados">Mensajes</a></li>
				<li><a href="logout">Cerrar sesi�n</a></li>
			</ul>
		</div>
	</header>
	<div id="private">
		<c:forEach var="post" items="${privados}">
		<div id="mensajes">
			<b>De:</b> <a href="<c:out value="${post.sender.id}"/>"><c:out value="${post.sender.name}"/></a>
			<b> Para:</b> <a href="<c:out value="${post.receiver.id}"/>"><c:out value="${post.receiver.name}"/></a><br>
			<b>Mensaje:</b> <c:out value="${post.body}"/><br>
			<b>Fecha:</b> <c:out value="${post.fecha}"/><hr>
			</div>
		</c:forEach>
	</div>
</body>
</html>