<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Timeline</title>
<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/css/style.css"/>" />
<script src="<c:url value="/resources/js/mensaje.js"/>"></script>
</head>
<body class="news">
	<header>
		<div class="nav">
			<ul>
				<li><a href="/1/timeline">Timeline</a></li>
				<li><a href="/1/<c:out value="${activeUser.id}"/>"><c:out
							value="${activeUser.name}" /></a></li>
				<li><a href="/1/categorias">Categor�as</a></li>
				<li><a href="/1/verprivados">Mensajes</a></li>
				<li><a href="/1/logout">Cerrar sesi�n</a></li>
			</ul>
		</div>
	</header>
	<div id="form">
		<form:form id="waterform" method="post" action="temporalpubli/${categoria.id}" modelAttribute="mensaje">

			<div class="formgroup" id="name-form">
				<label for="dayss">D�as de duraci�n del mensaje</label>
				<form:input type="number" id="days" name="days" path="dias" />
			</div>

			<div class="formgroup" id="message-form">
				<label for="message">Publicaci�n</label>
				<form:textarea id="message" name="message" path="body" />
			</div>

			<input type="submit" value="Enviar publicaci�n" />
		</form:form>
	</div>
	<div id="timeline">
		<c:forEach var="post" items="${listamensajes}">
			<b>Nombre:</b> <a href="<c:out value="${post.sender.id}"/>"><c:out value="${post.sender.name}"/></a><br>
			<b>Mensaje:</b> <c:out value="${post.body}"/><hr>
		</c:forEach>
	</div>
</body>
</html>