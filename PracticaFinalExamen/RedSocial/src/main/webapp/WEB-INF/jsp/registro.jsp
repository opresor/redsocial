<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Sign Up Form</title>
<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/css/loginreg.css"/>" />
</head>
<body>
<p class="texto">Registro</p>
<div class="Registro">
<form:form method="post" action="#" modelAttribute="usuario">

<span class="fontawesome-user"></span><form:input type="text" path="username" placeholder="Nombre de usuario" autocomplete="off"/>
<span class="fontawesome-user"></span><form:input type="text" path="name" placeholder="Nombre a mostrar" autocomplete="off"/>  
<span class="fontawesome-envelope-alt"></span><form:input type="text" path="email" id="email" placeholder="Correo" autocomplete="off"/>
<span class="fontawesome-lock"></span><form:input type="password" path="password" name="password" id="password" placeholder="Contraseņa" autocomplete="off"/> 
			<input type="submit" value="Registrar" title="Registra tu cuenta">
	</form:form>
</body>
</html>
