package es.unex.cum.mydai.redsocial.dao;


import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.Table;

import es.unex.cum.mydai.redsocial.vo.HashtagVO;

public class HashtagDAOImpl implements HashtagDAO {
	
	protected EntityManager entityManager;

	public HashtagDAOImpl(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	public HashtagVO create(HashtagVO hashtag) {
		this.entityManager.persist(hashtag);
		return hashtag;
	}

	public HashtagVO update(HashtagVO hashtag) {
		hashtag = this.entityManager.merge(hashtag);
		return hashtag;
	}

	public HashtagVO find(Long id) {
		return this.entityManager.find(HashtagVO.class, id);
	}

	public void delete(HashtagVO hashtag) {
		hashtag = this.entityManager.merge(hashtag);
		this.entityManager.remove(hashtag);

	}

	public HashtagVO findByHashtag(String hashtag) {
		Query query = this.entityManager.createQuery(
				"SELECT * FROM "+HashtagVO.class.getAnnotation(Table.class)+" WHERE tag = ?1", HashtagVO.class);
		query.setParameter(1, hashtag);
		return (HashtagVO) query.getSingleResult();
	}

}
