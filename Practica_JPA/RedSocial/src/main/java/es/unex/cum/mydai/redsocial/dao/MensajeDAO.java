package es.unex.cum.mydai.redsocial.dao;

import java.util.Set;

import es.unex.cum.mydai.redsocial.vo.MensajeVO;

public interface MensajeDAO {
	MensajeVO create(MensajeVO mensaje);
	MensajeVO find(Long id);
	MensajeVO update(MensajeVO mensaje);
	void delete(MensajeVO mensaje);
	Set<MensajeVO> findByUser(Long userid);
}
