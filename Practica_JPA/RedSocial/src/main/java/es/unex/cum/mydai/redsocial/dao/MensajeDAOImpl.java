package es.unex.cum.mydai.redsocial.dao;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import javax.persistence.Table;

import es.unex.cum.mydai.redsocial.vo.MensajeVO;

public class MensajeDAOImpl implements MensajeDAO {
	
	protected EntityManager entityManager;

	public MensajeDAOImpl(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	public MensajeVO create(MensajeVO mensaje) {
		this.entityManager.persist(mensaje);
		return mensaje;
	}

	public MensajeVO find(Long id) {
		return this.entityManager.find(MensajeVO.class, id);
	}

	public MensajeVO update(MensajeVO mensaje) {
		this.entityManager.merge(mensaje);
		return mensaje;
	}

	public void delete(MensajeVO mensaje) {
		mensaje = this.entityManager.merge(mensaje);
		this.entityManager.remove(mensaje);

	}

	@SuppressWarnings("unchecked")
	public Set<MensajeVO> findByUser(Long userid) {
		Query query = this.entityManager.createQuery(
				"SELECT * FROM "+MensajeVO.class.getAnnotation(Table.class).name()+" WHERE id_sender = ?1 ORDER BY fecha_envio DESC", MensajeVO.class);
		query.setParameter(1, userid);
		return new HashSet<MensajeVO>(query.getResultList());
	}
}
