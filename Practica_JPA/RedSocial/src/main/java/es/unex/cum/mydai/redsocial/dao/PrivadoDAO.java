package es.unex.cum.mydai.redsocial.dao;

import java.util.Set;

import es.unex.cum.mydai.redsocial.vo.PrivadoVO;

public interface PrivadoDAO {
	PrivadoVO create(PrivadoVO mensaje);
	PrivadoVO find(Long id);
	PrivadoVO update(PrivadoVO mensaje);
	void delete(PrivadoVO mensaje);
	Set<PrivadoVO> findConversation(Long idsender, Long idreceiver);
}
