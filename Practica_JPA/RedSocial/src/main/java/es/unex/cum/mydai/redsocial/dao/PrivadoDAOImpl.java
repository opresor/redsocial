package es.unex.cum.mydai.redsocial.dao;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.Table;

import es.unex.cum.mydai.redsocial.vo.PrivadoVO;

public class PrivadoDAOImpl implements PrivadoDAO {
	
	protected EntityManager entityManager;

	public PrivadoDAOImpl(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	public PrivadoVO create(PrivadoVO mensaje) {
		this.entityManager.persist(mensaje);
		return mensaje;
	}

	public PrivadoVO find(Long id) {
		return this.entityManager.find(PrivadoVO.class, id);
	}

	public PrivadoVO update(PrivadoVO mensaje) {
		this.entityManager.merge(mensaje);
		return mensaje;
	}

	public void delete(PrivadoVO mensaje) {
		mensaje = this.entityManager.merge(mensaje);
		this.entityManager.remove(mensaje);
	}

	@SuppressWarnings("unchecked")
	public Set<PrivadoVO> findConversation(Long idsender, Long idreceiver) {
		Query query = this.entityManager.createQuery(
				"SELECT * FROM "+PrivadoVO.class.getAnnotation(Table.class).name()+" WHERE id_sender = ?1 AND id_receiver = ?2 ORDER BY fecha_envio DESC",
				PrivadoVO.class);
		return new HashSet<PrivadoVO>(query.getResultList());
	}

}
