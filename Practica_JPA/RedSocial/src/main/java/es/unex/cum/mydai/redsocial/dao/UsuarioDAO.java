package es.unex.cum.mydai.redsocial.dao;

import java.util.Set;

import es.unex.cum.mydai.redsocial.vo.UsuarioVO;

public interface UsuarioDAO {
	UsuarioVO create(UsuarioVO usuario);
	UsuarioVO read(Long id);
	UsuarioVO update(UsuarioVO usuario);
	void delete(UsuarioVO usuario);
	Set<UsuarioVO> findByName(String name);
}
