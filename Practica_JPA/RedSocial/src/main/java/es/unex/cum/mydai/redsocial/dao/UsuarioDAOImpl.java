package es.unex.cum.mydai.redsocial.dao;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.Table;

import es.unex.cum.mydai.redsocial.vo.UsuarioVO;

public class UsuarioDAOImpl implements UsuarioDAO {
	
	protected EntityManager entityManager;
	
	public UsuarioDAOImpl(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	public UsuarioVO create(UsuarioVO usuario) {
		this.entityManager.persist(usuario);
		return usuario;
	}

	public UsuarioVO read(Long id) {
		return this.entityManager.find(UsuarioVO.class, id);
	}

	public UsuarioVO update(UsuarioVO usuario) {
		this.entityManager.merge(usuario);
		return usuario;
	}

	public void delete(UsuarioVO usuario) {
		usuario = this.entityManager.merge(usuario);
		this.entityManager.remove(usuario);
	}

	@SuppressWarnings("unchecked")
	public Set<UsuarioVO> findByName(String name) {
		Query query = entityManager.createQuery(
				"SELECT * FROM "+UsuarioVO.class.getAnnotation(Table.class).name()+" WHERE name LIKE ?1 OR username LIKE ?2", UsuarioVO.class);
		query.setParameter(1, "%"+name+"%");
		query.setParameter(2, "%"+name+"%");
		return new HashSet<UsuarioVO>(query.getResultList());
	}

}
