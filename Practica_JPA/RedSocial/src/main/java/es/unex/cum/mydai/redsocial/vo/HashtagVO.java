package es.unex.cum.mydai.redsocial.vo;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "HASHTAG")
public class HashtagVO implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -3858304173847483832L;
	private Long id;
	private String tag;
	private Set<MensajeVO> mensajes;
	
	public HashtagVO() {}
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	@Column(name = "TAG")
	public String getTag() {
		return tag;
	}
	public void setTag(String tag) {
		this.tag = tag;
	}
	
	@OneToMany(mappedBy = "hashtag")
	public Set<MensajeVO> getMensajes() {
		return mensajes;
	}
	public void setMensajes(Set<MensajeVO> mensajes) {
		this.mensajes = mensajes;
	}
	
	

}
