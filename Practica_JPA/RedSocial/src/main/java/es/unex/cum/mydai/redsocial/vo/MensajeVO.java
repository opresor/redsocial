package es.unex.cum.mydai.redsocial.vo;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
@Entity
@Table(name = "MENSAJES")
public class MensajeVO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2819136255644301650L;
	private Long id;
	private UsuarioVO sender;
	private String body;
	private Date fecha;
	private HashtagVO hashtag;
	
	public MensajeVO() {}
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	@OneToOne(cascade=CascadeType.PERSIST)
	@JoinColumn(name = "ID_SENDER")
	public UsuarioVO getSender() {
		return sender;
	}

	public void setSender(UsuarioVO sender) {
		this.sender = sender;
	}
	
	@Column(name = "BODY")
	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHA_ENVIO")
	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_HASHTAG")
	public HashtagVO getHashtag() {
		return hashtag;
	}

	public void setHashtag(HashtagVO hashtag) {
		this.hashtag = hashtag;
	}
	
	
}
