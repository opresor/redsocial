package es.unex.cum.mydai.redsocial.vo;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "PRIVADOS")
public class PrivadoVO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 4180546986969204710L;
	private Long id;
	private UsuarioVO sender;
	private UsuarioVO receiver;
	private String body;
	private Date fecha;
	
	public PrivadoVO() {}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@OneToOne(cascade=CascadeType.PERSIST)
	@JoinColumn(name = "ID_SENDER")
	public UsuarioVO getSender() {
		return sender;
	}

	public void setSender(UsuarioVO sender) {
		this.sender = sender;
	}

	@OneToOne(cascade=CascadeType.PERSIST)
	@JoinColumn(name = "ID_RECEIVER")
	public UsuarioVO getReceiver() {
		return receiver;
	}

	public void setReceiver(UsuarioVO receiver) {
		this.receiver = receiver;
	}
	
	@Column(name = "BODY")
	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHA_ENVIO")
	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
	
	
}
