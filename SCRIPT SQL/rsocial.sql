-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 03-07-2019 a las 12:51:10
-- Versión del servidor: 10.1.40-MariaDB
-- Versión de PHP: 7.3.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `rsocial`
--
CREATE DATABASE IF NOT EXISTS `rsocial` DEFAULT CHARACTER SET utf8 COLLATE utf8_spanish_ci;
USE `rsocial`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categoria`
--

CREATE TABLE `categoria` (
  `id` bigint(20) NOT NULL,
  `NOMBRE` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `categoria`
--

INSERT INTO `categoria` (`id`, `NOMBRE`) VALUES
(1, 'humor'),
(2, 'trabajo'),
(3, 'noticias'),
(4, 'economia'),
(5, 'aficiones'),
(6, 'viajes');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `hashtag`
--

CREATE TABLE `hashtag` (
  `id` bigint(20) NOT NULL,
  `TAG` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `hashtag`
--

INSERT INTO `hashtag` (`id`, `TAG`) VALUES
(1, 'oladecalor'),
(2, 'examenes');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mensajes`
--

CREATE TABLE `mensajes` (
  `id` bigint(20) NOT NULL,
  `BODY` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `FECHA_ENVIO` datetime DEFAULT NULL,
  `ID_HASHTAG` bigint(20) DEFAULT NULL,
  `ID_SENDER` bigint(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `mensajes`
--

INSERT INTO `mensajes` (`id`, `BODY`, `FECHA_ENVIO`, `ID_HASHTAG`, `ID_SENDER`) VALUES
(1, '¡Que calor hace en Merida!', '2019-07-01 00:35:45', 1, 1),
(2, 'Animo en estos dias de examenes!', '2019-07-01 00:36:27', 1, 2),
(3, 'Ultimo esfuerzo!', '2019-07-01 09:31:07', 2, 1),
(4, '¡Uno más y se acabaron los exámanes!', '2019-07-01 09:57:04', 2, 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `privados`
--

CREATE TABLE `privados` (
  `id` bigint(20) NOT NULL,
  `BODY` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `FECHA_ENVIO` datetime DEFAULT NULL,
  `ID_RECEIVER` bigint(20) DEFAULT NULL,
  `ID_SENDER` bigint(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `privados`
--

INSERT INTO `privados` (`id`, `BODY`, `FECHA_ENVIO`, `ID_RECEIVER`, `ID_SENDER`) VALUES
(1, 'Holaaaaaaaaaa', '2019-07-01 00:43:25', 2, 1),
(2, 'Prueba prueba prueba prueba', '2019-07-01 09:17:10', 1, 2),
(3, 'Mucha suerte con tus exámenes Alberto', '2019-07-01 10:00:16', 3, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `temporales`
--

CREATE TABLE `temporales` (
  `id` bigint(20) NOT NULL,
  `BODY` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `FECHA_MAX` datetime DEFAULT NULL,
  `ID_CATEGORIA` bigint(20) DEFAULT NULL,
  `ID_SENDER` bigint(20) DEFAULT NULL,
  `DISPONIBILIDAD` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `temporales`
--

INSERT INTO `temporales` (`id`, `BODY`, `FECHA_MAX`, `ID_CATEGORIA`, `ID_SENDER`, `DISPONIBILIDAD`) VALUES
(1, 'Un día más y a casa!', '2019-07-10 10:42:25', 2, 1, 7);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user_follow`
--

CREATE TABLE `user_follow` (
  `FOLLOWED_ID` bigint(20) NOT NULL,
  `FOLLOWER_ID` bigint(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `user_follow`
--

INSERT INTO `user_follow` (`FOLLOWED_ID`, `FOLLOWER_ID`) VALUES
(1, 2),
(1, 3),
(2, 1),
(2, 3),
(3, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `id` bigint(20) NOT NULL,
  `AVATAR_IMG` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `EMAIL` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `FECHA_REGISTRO` datetime DEFAULT NULL,
  `NAME` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `PASSWORD` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `USERNAME` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id`, `AVATAR_IMG`, `EMAIL`, `FECHA_REGISTRO`, `NAME`, `PASSWORD`, `USERNAME`) VALUES
(1, NULL, 'carlos@unex.es', '2019-07-01 00:35:23', 'Opresor', 'carlos', 'carloscg'),
(2, NULL, 'unex@unex.es', '2019-07-01 00:36:13', 'UNEX', 'unex', 'unex'),
(3, NULL, 'alberto@unex.es', '2019-07-01 09:56:41', 'Alberto', 'alberto', 'albertounex');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `categoria`
--
ALTER TABLE `categoria`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `hashtag`
--
ALTER TABLE `hashtag`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `mensajes`
--
ALTER TABLE `mensajes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FKkndmii7aqaa3k90f4iq9p6b2g` (`ID_HASHTAG`),
  ADD KEY `FK2sdfs9bggqb3fl9x657d5wec5` (`ID_SENDER`);

--
-- Indices de la tabla `privados`
--
ALTER TABLE `privados`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK4jmv96xp15i5tbexrqpp9esgf` (`ID_RECEIVER`),
  ADD KEY `FKfp9yivtpy74jje35glixueta9` (`ID_SENDER`);

--
-- Indices de la tabla `temporales`
--
ALTER TABLE `temporales`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK9mqwjwl0v5tshgr65gu98or3e` (`ID_CATEGORIA`),
  ADD KEY `FK9nogn1m16fj51dw5i7wjljt8u` (`ID_SENDER`);

--
-- Indices de la tabla `user_follow`
--
ALTER TABLE `user_follow`
  ADD PRIMARY KEY (`FOLLOWED_ID`,`FOLLOWER_ID`),
  ADD KEY `FKkqjyhcrfbxmbpb1kt6y1w3nyr` (`FOLLOWER_ID`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `categoria`
--
ALTER TABLE `categoria`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `hashtag`
--
ALTER TABLE `hashtag`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `mensajes`
--
ALTER TABLE `mensajes`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `privados`
--
ALTER TABLE `privados`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `temporales`
--
ALTER TABLE `temporales`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
